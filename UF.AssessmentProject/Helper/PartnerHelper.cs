﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.AssessmentProject.Model;

namespace UF.AssessmentProject.Helper
{
    public static class PartnerHelper
    {
        public static List<Partner> GetPartners()
        {
            List<Partner> result = new List<Partner>() {
                new Partner() { partnerkey = "FAKEGOOGLE", partnerpassword = "FAKEPASSWORD1234" },
                new Partner() { partnerkey = "FAKEPEOPLE", partnerpassword = "FAKEPASSWORD4578" },
            };

            return result;
        }
    }
}
