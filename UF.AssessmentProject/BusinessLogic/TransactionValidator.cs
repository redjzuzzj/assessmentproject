﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UF.AssessmentProject.Helper;
using UF.AssessmentProject.Model;

namespace UF.AssessmentProject.BusinessLogic
{
    public static class TransactionValidator
    {
        public static bool ValidatePartner(Model.Transaction.RequestMessage req)
        {
            bool result = true;

            List<Partner> partners = PartnerHelper.GetPartners();

            Partner partnerRecord = partners.FirstOrDefault(x => x.partnerkey == req.partnerkey); // find a valid partner

            if(partnerRecord != null) {
                string decodedPartnerPassword = EncodingHelper.Base64Decode(req.partnerpassword);
                if (partnerRecord.partnerpassword != decodedPartnerPassword) { // is partner found with a valid password
                    result = false;
                }
            } else {
                result = false;
            }

            return result;
        }

        public static bool ValidateSignature(Model.Transaction.RequestMessage req)
        {
            bool result = true;

            bool isPartnerValid = ValidatePartner(req);

            if(isPartnerValid == true) {

                var formattedTimestamp = req.RealTimeStamp.ToString("yyyyMMddHHmmss");

                string rawSignatureValue = string.Format("{0}{1}{2}{3}{4}", formattedTimestamp, req.partnerkey, req.partnerrefno, req.totalamount, req.partnerpassword);

                string encryptedSignatureValue = EncryptionHelper.GetSha256Hash(rawSignatureValue);

                string encodedSignatureValue = EncodingHelper.Base64Encode(encryptedSignatureValue);

                if (req.sig != encodedSignatureValue) { 
                    result = false; 
                }

            } else {
                result = false;
            }

            return result;
        }                   

        public static bool ValidateRequstTimestamp(Model.Transaction.RequestMessage req)
        {
            bool result = true;

            DateTime serverTimestamp = DateTime.Now;
            DateTime requestTimestamp = req.RealTimeStamp;

            int comparisonTimestamp = serverTimestamp.CompareTo(requestTimestamp);

            if (comparisonTimestamp > 0) { // server time is in advance
                var x = serverTimestamp.Subtract(requestTimestamp);
                if(x.Minutes > 5) {
                    result = false;
                }
            } else if (comparisonTimestamp < 0) { // request time is in advance
                var x = requestTimestamp.Subtract(serverTimestamp);
                if (x.Minutes > 5)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool ValidateTransactionAmount(Model.Transaction.RequestMessage req)
        {
            bool result = true;
            if (req.items.Count > 0) {
                var totalAmountOfItems = req.items.Sum(x => {
                    return (x.unitprice * x.qty);
                });
                if (req.totalamount != totalAmountOfItems) {
                    result = false;
                }
            }
            return result;
        }

        public static ValidationResult ValidateCurrencyValue(long value)
        {
            ValidationResult result = ValidationResult.Success;

            if (value < 0) {
                result = new ValidationResult("Only positive value is allowed.");
            }

            return result;

        }

        public static ValidationResult ValidateTimeStamp(string value)
        {
            ValidationResult result = ValidationResult.Success;

            DateTime parsedDateTimeStamp = new DateTime();
            bool parseResult = DateTime.TryParseExact(value, "O", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.RoundtripKind, out parsedDateTimeStamp);
            if (parseResult != true) {
                result = new ValidationResult("Invalid date ISO 8601 format of timestamp.");
            }

            return result;

        }

    }
}
