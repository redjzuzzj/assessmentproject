﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.AssessmentProject.BusinessLogic;

namespace UF.AssessmentProject.Controllers
{
    [Produces("application/json"),
        Route("api/[action]"),
        ApiController]
    [SwaggerTag("Transaction Middleware Controller to keep transactional data in Log Files")]
    public class TransactionController : ControllerBase
    {
        /// <summary>
        /// Submit Transaction data
        /// </summary>
        /// <remarks>
        /// Ensure all parameter needed and responded as per IDD
        /// Ensure all posible validation is done
        /// API purpose: To ensure all data is validated and only valid partner with valid signature are able to access to this API
        /// </remarks>
        /// <param name="req">language:en-US(English), ms-MY(BM)</param>  
        /// <returns></returns>
        [HttpPut]
        [SwaggerResponse(StatusCodes.Status200OK, "Submit Transaction Message successfully", typeof(Model.Transaction.ResponseMessage))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized, Request")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Oops! Can't get your Post right now")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid parameter value")]
        public ActionResult<Model.Transaction.ResponseMessage> SubmitTRansaction(Model.Transaction.RequestMessage req)
        {
            Model.Transaction.ResponseMessage results = new Model.Transaction.ResponseMessage() { success = Helper.DataDictionary.responseResult.success };


            bool isPartnerValid = TransactionValidator.ValidatePartner(req);
            if (isPartnerValid != true) {
                results.success = Helper.DataDictionary.responseResult.failed;
                results.errormessage = "Access Denied!";
                return Ok(results);
            }

            bool isSignatureValid = TransactionValidator.ValidateSignature(req);
            if (isSignatureValid != true) {
                results.success = Helper.DataDictionary.responseResult.failed;
                results.errormessage = "Access Denied!";
                return Ok(results);
            }

            bool isTimestampValid = TransactionValidator.ValidateRequstTimestamp(req);
            if (isTimestampValid != true) {
                results.success = Helper.DataDictionary.responseResult.failed;
                results.errormessage = "Expired";
                return Ok(results);
            }

            bool isTransactionAmountValis = TransactionValidator.ValidateTransactionAmount(req);
            if (isTransactionAmountValis != true)
            {
                results.success = Helper.DataDictionary.responseResult.failed;
                results.errormessage = "Invalid Total Amount";
                return Ok(results);
            }

            return Ok(results);
        }

    }
}
