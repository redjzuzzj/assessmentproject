﻿using System.ComponentModel.DataAnnotations;
using UF.AssessmentProject.BusinessLogic;

namespace UF.AssessmentProject.Model.Transaction
{
    public class Itemdetail
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        [StringLength(50, ErrorMessage = "Invalid character length, maximum allowed character is 50.")]
        public string partneritemref { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        [StringLength(100, ErrorMessage = "Invalid character length, maximum allowed character is 100.")]
        public string name { get; set; }

        [Range(1, 5, ErrorMessage = "{0} must be between 1 to 5")]
        public int qty { get; set; }

        [CustomValidation(typeof(TransactionValidator), nameof(TransactionValidator.ValidateCurrencyValue))]
        public long unitprice { get; set; }
    }
}
