﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UF.AssessmentProject.BusinessLogic;

namespace UF.AssessmentProject.Model.Transaction
{
    public class RequestMessage : Model.RequestMessage
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        [StringLength(50, ErrorMessage = "Invalid character length maximum character is 50.")]
        public string partnerrefno { get; set; }

        [Required(ErrorMessage = "{0} is Required.")]
        [CustomValidation(typeof(TransactionValidator), nameof(TransactionValidator.ValidateCurrencyValue))]
        public long totalamount { get; set; }

        [Required(ErrorMessage = "{0} is Required.")]
        [MinLength(1, ErrorMessage = "Number of {0} bought must be atleast 1 and above")]
        public List<Itemdetail> items { get; set; }
    }

    public class ResponseMessage: Model.ResponseMessage
    {
    }
}
