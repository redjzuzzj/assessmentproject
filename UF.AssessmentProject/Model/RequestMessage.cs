﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UF.AssessmentProject.BusinessLogic;
using UF.AssessmentProject.Helper;

namespace UF.AssessmentProject.Model
{
    public class RequestMessage
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        [StringLength(50, ErrorMessage = "Invalid character length maximum character is 50.")]
        public string partnerkey { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        public string sig { get; set; } 

        /// <summary>
        /// Time in the ISO format. ie. 2014-04-14T12:34:23.00+0800 Preferdably set using RealTimeStamp instead.
        /// </summary>
        /// <example>2020-07-28T12:34:23.00+0800</example>
        /// <returns></returns> 
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        [CustomValidation(typeof(TransactionValidator), nameof(TransactionValidator.ValidateTimeStamp))]
        public string timestamp { get; set; }

        private DateTime _realTimeStamp;
        /// <summary>
        /// When this field is set, automatically converts to the string 
        /// for the timestamp property
        /// </summary> 
        /// <remarks>Internal use</remarks>
        [Newtonsoft.Json.JsonIgnore()]
        [System.Text.Json.Serialization.JsonIgnore()]
        public DateTime RealTimeStamp
        {
            get {
                DateTime parsedDateTimeStamp;
                bool parseResult = DateTime.TryParse(timestamp, out parsedDateTimeStamp);
                if (parseResult == true) {
                    _realTimeStamp = DateTime.Parse(timestamp, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.RoundtripKind);
                } else {
                    _realTimeStamp = DateTime.Parse(DateTime.Now.ToLocalTime().ToString("o"), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.RoundtripKind);
                }
                return _realTimeStamp;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} is Required.")]
        public string partnerpassword { get; set; }

    }

}
