﻿using System;
using System.Collections.Generic;
using System.Linq;
using UF.AssessmentProject.BusinessLogic;
using UF.AssessmentProject.Helper;
using UF.AssessmentProject.Model;
using UF.AssessmentProject.Model.Transaction;
using Xunit;
namespace AssessmentUnitTest
{
    public class ModelStateUnitTest
    {
        [Fact]
        public void RequestMessageModel_ExpectedModelState_True()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.True(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_partnerkey_EmptryString_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = "",
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_partnerkey_Exceed50Character_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_partnerpassword_EmptryString_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = "",
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_partnerrefno_EmptryString_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_partnerrefno_Exceed50Character_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "partnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefnopartnerrefno",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_sig_EmptryString_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_timestamp_EmptryString_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_timestamp_InvalidFormat_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_totalamount_NegativeValue_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = -500,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_items_EmptyList_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>()
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void RequestMessageModel_items_IsNull_ExpectedModelState_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = null
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(request, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(request, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_ExpectedModelState_True()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "Pen",
                partneritemref = "i-00001",
                qty = 2,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.True(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_name_EmptryString_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "",
                partneritemref = "i-00001",
                qty = 2,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_name_Exceed100Character_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
                partneritemref = "i-00001",
                qty = 2,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_partneritemref_EmptryString_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "Pen",
                partneritemref = "",
                qty = 2,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_partneritemref_Exceed50Character_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "Pen",
                partneritemref = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz",
                qty = 2,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_qty_LessThan_1_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "Pen",
                partneritemref = "i-00001",
                qty = 0,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_qty_Exceed_5_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "Pen",
                partneritemref = "i-00001",
                qty = 6,
                unitprice = 250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }

        [Fact]
        public void ItemdetailModel_unitprice_NegativeValue_ExpectedModelState_False()
        {

            Itemdetail item = new Itemdetail()
            {
                name = "Pen",
                partneritemref = "i-00001",
                qty = 2,
                unitprice = -250
            };

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(item, null, null);
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            var isModelStateValid = System.ComponentModel.DataAnnotations.Validator.TryValidateObject(item, context, results, true);

            Assert.False(isModelStateValid);
        }
    }
}
