using System;
using System.Collections.Generic;
using System.Linq;
using UF.AssessmentProject.BusinessLogic;
using UF.AssessmentProject.Helper;
using UF.AssessmentProject.Model;
using UF.AssessmentProject.Model.Transaction;
using Xunit;

namespace AssessmentUnitTest
{
    public class TransactionUnitTest
    {
        [Fact]
        public void ValidatePartner_Return_PartnerIsValid_True()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();

            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage() { 
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() { 
                    new Itemdetail() { 
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            bool partnerIsValid = TransactionValidator.ValidatePartner(request);

            Assert.True(partnerIsValid);
        }

        [Fact]
        public void ValidatePartner_Return_PartnerIsValid_False()
        {

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = "INVALID_PARTNER",
                partnerpassword = "INVALID_PASSWORD",
                partnerrefno = "FG-00001",
                sig = "24XYSmvKGH9I9Y5FLvSsId2MPtjkvog7U5JLhE3m30A=",
                timestamp = "2013-11-22T02:11:22.0000000Z",
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            bool partnerIsValid = TransactionValidator.ValidatePartner(request);

            Assert.False(partnerIsValid);
        }

        [Fact]
        public void ValidateSignature_Return_SignatureIsValid_True()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();
            Partner selectedPartner = avaliablePartners.First();

            string transactionDate = DateTime.Now.ToUniversalTime().ToString("o");

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "",
                timestamp = transactionDate,
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };
            
            // generate signature start
            var formattedTimestamp = request.RealTimeStamp.ToString("yyyyMMddHHmmss");
            string rawSignatureValue = string.Format("{0}{1}{2}{3}{4}", formattedTimestamp, request.partnerkey, request.partnerrefno, request.totalamount, request.partnerpassword);
            string encryptedSignatureValue = EncryptionHelper.GetSha256Hash(rawSignatureValue);
            string transactionSignature = EncodingHelper.Base64Encode(encryptedSignatureValue);
            // generate signature end

            request.sig = transactionSignature;

            bool signatureIsValid = TransactionValidator.ValidateSignature(request);

            Assert.True(signatureIsValid);
        }

        [Fact]
        public void ValidateSignature_Return_SignatureIsValid_False()
        {

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();
            Partner selectedPartner = avaliablePartners.First();

            string transactionDate = DateTime.Now.ToUniversalTime().ToString("o");

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "",
                timestamp = transactionDate,
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            // generate signature start
            var formattedTimestamp = request.RealTimeStamp.ToString("yyyyMMddHHmmss");
            string rawSignatureValue = string.Format("{0}{1}{2}{3}{4}", formattedTimestamp, "INVALID_PARTHER_KEY", request.partnerrefno, request.totalamount, request.partnerpassword);
            string encryptedSignatureValue = EncryptionHelper.GetSha256Hash(rawSignatureValue);
            string transactionSignature = EncodingHelper.Base64Encode(encryptedSignatureValue);
            // generate signature end

            request.sig = transactionSignature;

            bool signatureIsValid = TransactionValidator.ValidateSignature(request);

            Assert.False(signatureIsValid);
        }

        [Fact]
        public void ValidateRequstTimestamp_EqualTimestamp_Return_TransactionTimestampIsValid_True()
        {
            string transactionDate = DateTime.Now.ToLocalTime().ToString("o"); // same timestamp with server

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();
            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "",
                timestamp = transactionDate,
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            // generate signature start
            var formattedTimestamp = request.RealTimeStamp.ToString("yyyyMMddHHmmss");
            string rawSignatureValue = string.Format("{0}{1}{2}{3}{4}", formattedTimestamp, request.partnerkey, request.partnerrefno, request.totalamount, request.partnerpassword);
            string encryptedSignatureValue = EncryptionHelper.GetSha256Hash(rawSignatureValue);
            string transactionSignature = EncodingHelper.Base64Encode(encryptedSignatureValue);
            // generate signature end

            request.sig = transactionSignature;

            bool transactionTimestapIsValid = TransactionValidator.ValidateRequstTimestamp(request);

            Assert.True(transactionTimestapIsValid);
        }

        [Fact]
        public void ValidateRequstTimestamp_AdvanceTimestamp_Return_TransactionTimestampIsValid_False()
        {
            string transactionDate = DateTime.Now.ToLocalTime().AddMinutes(10).ToString("o"); // Request advance 10 minutes than server timestamp

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();
            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "",
                timestamp = transactionDate,
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            // generate signature start
            var formattedTimestamp = request.RealTimeStamp.ToString("yyyyMMddHHmmss");
            string rawSignatureValue = string.Format("{0}{1}{2}{3}{4}", formattedTimestamp, request.partnerkey, request.partnerrefno, request.totalamount, request.partnerpassword);
            string encryptedSignatureValue = EncryptionHelper.GetSha256Hash(rawSignatureValue);
            string transactionSignature = EncodingHelper.Base64Encode(encryptedSignatureValue);
            // generate signature end

            request.sig = transactionSignature;

            bool transactionTimestapIsValid = TransactionValidator.ValidateRequstTimestamp(request);

            Assert.False(transactionTimestapIsValid);
        }

        [Fact]
        public void ValidateRequstTimestamp_LateTimestamp_Return_TransactionTimestampIsValid_False()
        {
            string transactionDate = DateTime.Now.ToLocalTime().AddMinutes(-10).ToString("o"); // Request advance 10 minutes than server timestamp

            List<Partner> avaliablePartners = PartnerHelper.GetPartners();
            Partner selectedPartner = avaliablePartners.First();

            UF.AssessmentProject.Model.Transaction.RequestMessage request = new UF.AssessmentProject.Model.Transaction.RequestMessage()
            {
                partnerkey = selectedPartner.partnerkey,
                partnerpassword = EncodingHelper.Base64Encode(selectedPartner.partnerpassword),
                partnerrefno = "FG-00001",
                sig = "",
                timestamp = transactionDate,
                totalamount = 1000,
                items = new List<Itemdetail>() {
                    new Itemdetail() {
                        name = "Pen",
                        partneritemref = "i-00001",
                        qty = 2,
                        unitprice = 250
                    },
                    new Itemdetail() {
                        name = "Ruler",
                        partneritemref = "i-00002",
                        qty = 1,
                        unitprice = 500
                    }
                }
            };

            // generate signature start
            var formattedTimestamp = request.RealTimeStamp.ToString("yyyyMMddHHmmss");
            string rawSignatureValue = string.Format("{0}{1}{2}{3}{4}", formattedTimestamp, request.partnerkey, request.partnerrefno, request.totalamount, request.partnerpassword);
            string encryptedSignatureValue = EncryptionHelper.GetSha256Hash(rawSignatureValue);
            string transactionSignature = EncodingHelper.Base64Encode(encryptedSignatureValue);
            // generate signature end

            request.sig = transactionSignature;

            bool transactionTimestapIsValid = TransactionValidator.ValidateRequstTimestamp(request);

            Assert.False(transactionTimestapIsValid);
        }

    }
}
